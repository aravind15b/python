import json
import requests
import datetime

with requests.get("https://api.covid19india.org/data.json") as response:
    source = response.json()

for item in  source['statewise']:
    if(item['state'] == "Tamil Nadu"):
        active=item['active']
        confirmed=item['confirmed']
        deaths = item['deaths']
        lastupdatedtime=item['lastupdatedtime']
        break
if (lastupdatedtime == '' or lastupdatedtime==None):
    lastupdatedtime = datetime.date.today()-datetime.timedelta(1) 
    lastupdatedtime = lastupdatedtime.strftime('%d-%m-%Y')   
print(f'active:{active}, confirmed:{confirmed}, deaths:{deaths},lastupdatedtime:{lastupdatedtime}')

