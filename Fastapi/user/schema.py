from pydantic.main import BaseModel
 
class CreateUser(BaseModel):
    email:str
    password: str

    class Config:
        orm_mode =True