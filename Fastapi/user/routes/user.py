import sys


sys.path.append("..")
from fileinput import filename

import shutil


import email
from fastapi import Depends, HTTPException, APIRouter,UploadFile,File
from database import engine, SessionLocal
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from user.models import User
from user import models,schema
bcrypt_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
router = APIRouter(
    prefix="/user",
    tags=["user"],
)




def get_password_hash(password):
    return bcrypt_context.hash(password)

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

@router.post("/")
async def create_user(password:str,email:str,file:UploadFile=File(...),db: Session = Depends(get_db)):
   
    hash_password = get_password_hash(password)

    with open("Images/"+file.filename,"wb")as Image:
        shutil.copyfileobj(file.file,Image)

    image_url= str("Images/"+file.filename)
    create_user_model = models.User(email=email,hased_password=hash_password,image_url=image_url)
    db.add(create_user_model)
    db.commit()
    return {
        'status': 200,
        'transaction': 'Successful'
    }

@router.get("/")
async def read_all(db: Session = Depends(get_db)):
    return db.query(User).all()

@router.get("/{id}")
async def get_userid(id:int,db: Session = Depends(get_db)):
    if id is None:
        raise HTTPException(status_code=400,detail="user not found")
    get_user_id = db.query(models.User).filter(models.User.id==user_id).first()
    if get_user_id is None:
        raise HTTPException(status_code=400,detail="user not found")
    return get_user_id

@router.put("/{id}")
async def update_user(id:int,user:schema.CreateUser,db: Session = Depends(get_db)):
    get_user = db.query(models.User).filter(models.User.id==id).first()
    if get_user is None:
        raise HTTPException(status_code=400,detail="user not found")
    hash_password = get_password_hash(user.password)
    get_user.hased_password =hash_password
    get_user.email=user.email
    db.commit()
    return {'status': 200,'transaction': 'Successful'}

@router.delete("/{id}")
async def delete_user(id:int,db: Session = Depends(get_db)):
    get_user_id = db.query(models.User).filter(models.User.id==id).first()
    
    print(get_user_id)
    if get_user_id is None:
        raise HTTPException(status_code=400,detail="user not found")
    
    db.query(models.User).filter(models.User.id==id).delete()
    db.commit()
    return {'status': 200,'transaction': 'Successful'}
    