from sqlalchemy import Column,String,DateTime,Integer,Boolean
from sqlalchemy_utils import URLType
from database import Base
import datetime

class User(Base):
    __tablename__= "users"

    id = Column(Integer, primary_key=True)
    email = Column(String, unique= True)
    hased_password = Column(String)
    image_url = Column(URLType)
    is_active = Column(Boolean, default=True)
    created_date = Column(DateTime,default=datetime.datetime.utcnow)
    updated_date = Column(DateTime,onupdate=datetime.datetime.now)
