from sqlite3 import DatabaseError
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL="postgresql://aravind:user@localhost/Ghost" 

engine = create_engine(SQLALCHEMY_DATABASE_URL)
print(engine)

SessionLocal = sessionmaker(autocommit=False, autoflush=False,bind=engine)
Base = declarative_base()